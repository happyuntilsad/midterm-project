#ifndef COMMANDUTIL_H
#define COMMANDUTIL_H

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "imageManip.h"
#include "ppmIO.h"

//sorts the input into various streams represented by ints negative returns correspond to errors, positive to functions
int checkSortFunctName(int argc, char* argv[]); //formerly was const char* argv[], however threw warnings when passed. Desinged program keeping it const for safety

//passes the arguments to their respective functions in imageManip.c
int passArg(int argc, char* argv[],int sort, Image* image);//also used to be const see above

#endif
