#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "ppmIO.h"
#include "imageManip.h"

int swapColor(Image* image) { //swaps colors 
  unsigned char temp1, temp2; // stores temp unsigned char values 
  int length = image->cols * image->rows; //length of image  
  for(int i = 0; i < length; i++) //performs the swap red to blue, green to red, blue to green
    {
      temp1 = image->data[i].b;
      temp2 = image->data[i].g;
      image->data[i].b = image->data[i].r;
      image->data[i].g = temp1;
      image->data[i].r = temp2;
    }
        return 0;
}

int greyScale(Image* image) { //grayscale function turns image grey
    int length = image->cols * image->rows;
    double  intensity; //stores value to be copeied across
    for(int i = 0; i < length; i++) { //performs grayscale on all pixels
       intensity = (image->data[i].r * .30) + (image->data[i].g * .59) + (image->data[i].b * .11); //given algorithmn
        image->data[i].r = intensity;
        image->data[i].g = intensity;
	image->data[i].b = intensity; 
    }
    return 0; 
}

void set_color(Image *image, int row_start, int row_end, int col_start, int col_end, const unsigned char color){ //helper for blackout
  for(int i = row_start; i < row_end; i++){ //pixel by pixel color change
    for(int j = col_start; j < col_end; j++){
      image->data[i*image->cols + j].r = color;
      image->data[i*image->cols + j].g = color;
      image->data[i*image->cols + j].b = color;
    }
  }
  return;
}

int blackout(Image* image) { //blacks out quarter of image
  int rows = image->rows;
  int cols = image->cols;
  const unsigned char black = 0; //blackout value
  //performs blackout by calling helper set_color
  if(rows % 2 == 0){
    if(cols % 2 == 0) {
      set_color(image, 0, rows/2, cols/2, cols, black);
    }
    else if(cols % 2 == 1) {
      set_color(image, 0, rows/2, (cols/2)+1, cols, black); 
    }
  }
  else if(rows % 2 == 1){
    if(cols % 2 == 0) {
      set_color(image, 0, (rows/2) - 1, cols/2, cols, black);
    }
    else if(cols % 2 == 1) {
      set_color(image, 0, (rows/2) - 1, (cols/2)+1, cols, black);
    }
  }
  return 0;
}


int crop(Image* image, int left_col, int top_row, int right_col, int bot_row) { //crops image given y1 x1 y2 x2 columns first
  if(top_row < 0 || bot_row > image->rows){
    fprintf(stderr, "[ERROR] Invalid row size.");
    return 7;
  }
  if(left_col < 0 || right_col > image->cols){
    fprintf(stderr, "[ERROR] Invalid column size.");
    return 7;
  } 
  
  int row_size = bot_row - top_row;
  int col_size = right_col - left_col;
  unsigned int crop_size = row_size * col_size;

  //Formats cropped_im with user specifications
  Image *cropped_im = (Image *) malloc(sizeof(Image));  
  cropped_im->data  = malloc(crop_size * sizeof(Pixel));
  cropped_im->rows  = row_size;
  cropped_im->cols  = col_size;

  //Copies image to cropped image
  for(int i=0, r_index = top_row; i<row_size; i++, r_index++){
    for(int j=0, c_index = left_col; j<col_size; j++, c_index++){
      cropped_im->data[(col_size)*i + j].r = image->data[(image->cols)*r_index + c_index].r;
      cropped_im->data[(col_size)*i + j].g = image->data[(image->cols)*r_index + c_index].g;
      cropped_im->data[(col_size)*i + j].b = image->data[(image->cols)*r_index + c_index].b;
    }  
  }
    
  // Resizes image to cropped image
  image->data = realloc((void*)image->data, crop_size * sizeof(Pixel));
  image->rows = cropped_im->rows;
  image->cols = cropped_im->cols;

  // Copies the cropped image to image
  for(unsigned int i=0; i < crop_size; i++){
    image->data[i].r = cropped_im->data[i].r;
    image->data[i].g = cropped_im->data[i].g;
    image->data[i].b = cropped_im->data[i].b;
  }
  
  free(cropped_im->data);
  free(cropped_im);
  
  return 0;
}

double convertTo(unsigned char c) //helper for contrast converts unsigned char to double 
{
    int convert = (int)c;
    double output; 
    double ratio; 

    if (convert <= 127) //conversion from [0, 127] places in range [-127, 0]
     {
       convert  -= 127;//redundant but allowed for modification in orginal form 
       ratio = convert; 
     }
    else //conversion from [128,255] places in range [0,127] asked TA said to use this instead of bounds of [-128, -1] and [1, 128] even though both 127 and 128 would be 0
    {
     convert -= 128; 
     ratio = convert;
    }
    output = ratio;
    return output; 
}

unsigned char convertBack(double d) //contrast helper converts double back to unsigned char
{ 
    double ratio = d; 
    int convert = 0;
    unsigned char output;

    if (d < 0 ) //handles range of [-127 , 0] from above (multiplied by value   
    { 
      ratio += 127; //opp of original operation
      convert = ratio;
    }
    else //handles range of [0 , 127] from above 
    {
      ratio += 128; //opp of original operation 
      convert = ratio;  
    }
    if (convert > 255) //returns color values to within upper bounds
    {
      convert = 255; //anything over 255 becomes 255
    }
    if (convert < 0) //returns values to within lower bounds
    {
      convert = 0; //anything below zero becomes zero
    }
    output = convert; 
    return output;
    
}

int contrast(Image *image, double adjFactor) //actual function for contrast 
{ 
    int length = image->cols * image->rows;
    double unsaturatedR, unsaturatedG, unsaturatedB;  
    for(int i = 0; i < length; i++) { //loop to call two helpers and perfrom contrast 
        unsaturatedR = convertTo(image->data[i].r);
        unsaturatedG = convertTo(image->data[i].g);
        unsaturatedB = convertTo(image->data[i].b);
        unsaturatedR *= adjFactor;
        unsaturatedG *= adjFactor;
        unsaturatedB *= adjFactor;
        image->data[i].r = convertBack(unsaturatedR);
        image->data[i].g = convertBack(unsaturatedG);
        image->data[i].b = convertBack(unsaturatedB);
    }
    return 0; 
}
