#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"

int checkSortFunctName(int argc, char* argv[]) //return an int to describe how to handle input error(negative) or function (positive) 
{   
    //Return value Summary Errors: -5 -> error 5 (function name); -6 -> error 6 (arguments to function)
    //Return Summary Functions: :1 -> swap, 2 -> blackout, 3 -> crop, 4 -> grayscale, 5 -> contrast
    //if the user types less than 4 items then no argument was included as arg is 4th item
    if (argc < 4) 
    {
        return -5; //no arguments for the function routes to error return     
    }
    //checks to see if user wants to swap 
    if (strcmp("swap", argv[3]) == 0)
    {      
      if (argc == 4)//checks to see if swap has 4 args (correct amount)   
        {
	  return 1;  //routes to stream 1 which handles swap
        } 
      else //if incorrect number of args
       {
	 return -6; //routes to error 6 as defined in provided table
       }     
    } 
    //checks to see if user wants blackout
    else if (strcmp("blackout",argv[3]) ==  0)
    {
      if ( argc == 4 ) //checks if number of args is correct
      {
        return 2; //routes to stream 2 blackout
      }
      else //if incorrect # of args 
      {
	return -6; //routes to error 6
      }	  
    }
    //checks if user wants to crop 
    else if ((strcmp("crop",argv[3]) == 0) && (argc == 8))
    {
      for(int i=4; i < 8; i++){
	for(int j=0; j<strlen(argv[i]); j++){
	  if(!isdigit(arg[i][j]))
	    return -6;
	}
      }
      
      return 3; //routes to stream 3 crop 
    }
        //if 
        //nameSort = 3; //routes to check 3 crop
    //}

    //checks if user wants to grayscale
    else if (strcmp("grayscale",argv[3]) == 0) 
    {
      if (argc == 4) //checsk # of args
      {
	return 4; //routes to stream 4 grayscale 
      }
      else //incorrect # of args 
      {
	return -6; //routes to error 6
      }
    } 

    //checks if user wants to contrast
    else if (strcmp("contrast",argv[3]) == 0)
    {
      if (argc != 5) //checks # of args (style diff from previous)
      {
	return -6; //routes to stream 6
      }
      for(unsigned int i=0; i<strlen(argv[5]); i++){
	if(arg[5][i] == '.')
	  continue;
	else if(!isdigit(arg[5][i]))
            return -6;
        }
        return 5; //routes to stream 5 contrast     
    } 
    else //catch all case for not having args that match
    {
      return -5; //routes to error 5 
    } 
}

int passArg(int argc, char* argv[],int nameSort, Image *image) 
{
  int x1, x2, y1, y2;//integer arguments to pass to crop 
  double intensity; //double to pass to contrast
  int output = 8; 
    switch(nameSort) {
        case -5: //no operation name or invalid operation name
	return 5; //return to main 
        break;

        case -6: //Incorrect number of arguments/arguments specified for given operation
        return 6;
        break; 

        case 1: //swaps color once 
	output = swapColor(image); //function call 0 return 
        return output; //return to main 0
        break;

        case 2: //blacksout image
	output = blackout(image); //function call 0 return 
        return output; //return to main 0
        break;

        case 3: //crop image 
	sscanf(argv[4], "%d", &y1); //read arg 1 of crop first column 
	sscanf(argv[5], "%d", &x1); //read arg 2 of crop first row
	sscanf(argv[6], "%d", &y2); //read arg 3  of crop second column 
	sscanf(argv[7], "%d", &x2); //read arg 4  of crop second row
        output = crop(image, y1, x1, y2, x2); //function call 0 return if success, 7 fail
        return output; //return to main 0 success, 7 fail 
        break;
        
        case 4: //grayscale image (function spelled wrong) 
	output = greyScale(image); //function call
        return output; //return to main 0
        break;

        case 5: //contrast image
	sscanf(argv[4], "%lf", &intensity); //read intensity from command line
        output = contrast(image, intensity); //function call
        return output; //return 0 to main 
        break;

        default:
	return output; //return 8 by default (weird case)  
    }
}
