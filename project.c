#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"

int main(int argc, char* argv[]) {
  FILE* file1 = NULL; 
  Image* image = NULL;
  FILE  *fp;
  int error = 0;
 
  if (argc < 3) //with files there should be 3 args so if none/not enough are passed then it will be < 3
  {
    fprintf(stderr, "[Error]: Expected file(s) to be passed to main.");
    return 1;
  }
  file1 = fopen(argv[1], "r"); //open read file
  fp = fopen(argv[2], "w"); //open write file

    if(!file1) //can't open file to read
      {
      fprintf( stderr , "[ERROR] Could not open file for reading: %s\n" , argv[1] );
      free(image -> data); //frees to prevent leak
      free(image);
      return 2;
      }
    if(!fp) //can't write
      {
      fprintf( stderr , "[ERROR] Could not open file for writing: %s\n", argv[2] );
      free(image -> data); //frees to prevent leaks
      free(image);
      return 4; 
      }
    
    image = readPPM(file1); //reads ppm using ppmIO function

    if (image == NULL) //null pointer means read failed
	{
	  fprintf(stderr, "Reading failed ppm file not formated correctly\n");
	  fclose(fp); //memory leak prevention
	  //free(image -> data don't do this as it causes segmentation fault
	  free(image);
	  return 3;
	}

    int nameSort = checkSortFunctName(argc, argv); //path handling for non file I/O errors and functions
    int test = passArg(argc, argv, nameSort, image); //sorting for what to print/return 

    if (test == 0) //return 0 from test means function was succesful in modifiying 
      {
	if(writePPM(image, fp) == 4) //write to user specified file, also checks for error in writing
	  {
	    fclose(fp);
	    error = 4;
	    fprintf(stderr, "Error writing file to image\n");
	  }
      }
      
    else if(test == 5) //5 means operator name had an error 
      {
	fprintf(stderr, "no operation name or invalid operation name\n");
	//writePPM(image, fp);
	fclose(fp);
	error =  5; 
      }
	  
    else if(test == 6) //args for functions/operators had an error
      {
	fprintf(stderr, "Incorrect args specified for operation\n");
	fclose(fp);
	error =  6; 
      }
    else if(test == 7) //crop had an error
      {
	fprintf(stderr, "Incorrect arguments for crop\n");
	fclose(fp);
	error = 7;
      }
	  
    else if (test == 8) //catch all error 
      {
	fprintf(stderr, "Error unknown, return 8\n");
	fclose(fp);
	error =  8; 
      }
 
  free(image -> data); //frees 
  free(image);
  return error;// return error 
}
      

