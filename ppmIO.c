#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "ppmIO.h"

//reads the header and extracts dimensions, checks format
int readPPMHeader(FILE *fp, int *rows, int *cols)
{
  char ch;
  int  maxval;

  if (fscanf(fp, "P%c\n", &ch) != 1 || ch != '6') //checks if begins with P6 
  {
    fprintf(stderr, "[ERROR]: File is not in ppm raw format => cannot read.\n");
    return 3; 
  }
  // skip all comments in ppm file 
  ch = getc(fp);
  while (ch == '#')
    {
      do 
	{
	  ch = getc(fp);
	} while (ch != '\n');	// eads to the end of the line 
      ch = getc(fp);            
    }
  
  if (!isdigit(ch)) { //checks if ending has digits
    fprintf(stderr, "[ERROR]: Cannot read header information.\n");
    return 3; 
  }
  
  ungetc(ch, fp);		//  put that digit back

  fscanf(fp, "%d%d%d\n", cols, rows, &maxval); //read the rows, cols, and maximum value for a data

  if (maxval != 255) //checks color value
    {
    fprintf(stderr, "[ERROR]: Image does not have proper color value.\n");
    return 3; //Could also be 8 
  }
  return 0; 
}

Image *readPPM(FILE *fp) //reads the ppm file returns an image struct pointer if success NULL if fail
{
  int rows = 0;
  int cols =0;
  int num = 0;
  unsigned int size = 0;
  
  Image *image = (Image *) malloc(sizeof(Image)); //allocates memory for image
  
  if (!image) { //see if memory allocated
    fprintf(stderr, "[ERROR]: Cannot allocate memory for new image.\n");
    free(image);
    return NULL;
  }
  if (!fp) { //check if file opened
    fprintf(stderr, "[ERROR]: Cannot open file for reading.\n");
    free(image);
    return NULL; 
  }

  //Image was not of ppm format
  if(readPPMHeader(fp, &rows, &cols) != 0) {
    free(image);
    fclose(fp);
    return NULL;
  }
  //fill in items for image 
  size         = (rows * cols);
  image->data  = malloc(size * sizeof(Pixel) ); //allocate for pixel array
  image->rows  = rows;
  image->cols  = cols;

  num = fread(image->data, sizeof(Pixel) ,size, fp); //since everything was allocated and declared reading takes one step

  if (num != (int)size) //checks if right number of items were written to image
    {
      fprintf(stderr, "[ERROR]: Cannot read image data from file.\n");
      fclose(fp);
      free(image -> data); //test this part to see if this free is needed 
      free(image);
      return NULL;
    }
  
  fclose(fp);

  return image;
}

int writePPM(const Image *image, FILE *fp)
{
  int num;
  unsigned int size = image->rows * image->cols;

  if (!fp) { //can file be opened
    printf("cannot open file for writing");
    return 4; 
  }

  fprintf(fp, "P6\n%d %d\n%d\n", image->cols, image->rows, 255); //prints formatting for header

  num = fwrite(image->data, sizeof(Pixel), size, fp); //writes from pixel array to ppm file

  if (num != (int)size) { //items written doesn't match 
    printf("cannot write image data to file"); //test to see if free needed here 
    return 4;
  }

  fclose(fp); //closes file in normal case
  return 0;
}
