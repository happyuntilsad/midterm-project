#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "ppmIO.h"

/*swap adjacent color values for the image*/
int swapColor(Image* img);

/*Converts image into greyscale*/ 
int greyScale(Image *img);

/*Blacks out top right corner of image*/
int blackout(Image* image);

/*applies contrast factor to the image*/
int contrast(Image *image, double adjFactor);

/*Crops image with specific size specifications*/
int crop(Image* image, int left_col, int top_row, int right_col, int bot_row);

/*helper function for blackout, allows pixel by pixel color change*/
void set_color(Image *image, int row_start, int row_end, int col_start, int col_end, const unsigned char color);

#endif


