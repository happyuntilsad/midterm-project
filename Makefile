# Sample Makefile

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

# Links together files needed to create executable
project: 	project.o	ppmIO.o		imageManip.o	commandUtil.o
	$(CC) -o	project	project.o	ppmIO.o		imageManip.o	commandUtil.o	

# Compiles mainFile.c to create mainFile.o
# Note that we list functions.h here as a file that
# main_file.c depends on, since main_file.c #includes it
project.o: project.c ppmIO.h imageManip.h commandUtil.h
	$(CC) $(CFLAGS) -c project.c

# Compiles functions.c to create functions.o
# Note that we list functions.h here as a file that
# functions.c depends on, since functions.c #includes it

#Input output handling
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c
#Algorithmns for image editing
imageManip.o: imageManip.h imageManip.c
	$(CC) $(CFLAGS) -c imageManip.c
#Handles user input
commandUtil.o: commandUtil.h commandUtil.c
	$(CC) $(CFLAFGS) -c commandUtil.c
# Removes all object files and the executable named main,
# so we can start fresh
clean:
	rm -f *.o main
